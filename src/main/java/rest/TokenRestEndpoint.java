package rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.awt.*;

import database.Customer;
import database.Database;

//Needs to interact with SOAP for bank contact.
@Path("/token/{tk}")
public class TokenRestEndpoint {
    Database db = Database.getInstance();

    @Consumes(MediaType.APPLICATION_JSON)
    //@Produces(MediaType.APPLICATION_JSON)
    @PUT
    public Response handleAddTokens(TokenHandleBody tokenHandleBody){
        for(String token : TokenHandleBody.tokens){
            db.addToken(token);
        }
        return Response.ok("Added tokens").build();
    }
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response handleGetToken(@PathParam("tk") String token){
        String tk = db.findToken(token).orElse(null);
        db.removeToken(tk);
        return Response.ok(tk).build();
    }
}

@XmlRootElement
class TokenHandleBody {
    @XmlElement
    public static String[] tokens;
}