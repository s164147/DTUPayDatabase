package rest;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.awt.*;

import database.Customer;
import database.Database;
import database.User;

//Needs to interact with SOAP for bank contact.
@Path("/finduser/{userID}")
public class CustomerRestEndpoint {
    Database db = Database.getInstance();

    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    @PUT
    public Response handleAddCustomer(HandleTransferBody handleTransferBody){
        db.addCustomer(HandleTransferBody.customer);
        return Response.ok("Added customer").build();
    }
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response handleGetCustomer(@PathParam("userID") String userID){
        User user = db.findCustomer(userID).orElse(null);
        if(user == null){
            return Response.noContent().build();
        }
        return Response.ok(user).build();
    }
}

@XmlRootElement
class HandleTransferBody {
    @XmlElement
    static Customer customer;
}