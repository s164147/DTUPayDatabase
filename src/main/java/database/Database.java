package database;


import java.util.LinkedList;
import java.util.Optional;

//DRY principle being violated horribly. Plz fix.
public final class Database {

    private static Database databaseInstance;
    private Database(){

    }

    public static Database getInstance() {
        if(databaseInstance == null) {
            databaseInstance = new Database();
        }

        return databaseInstance;
    }

    private LinkedList<User> users = new LinkedList<>();
    private LinkedList<String> tokens = new LinkedList<>();

    public void addCustomer(Customer a){
        users.add(a);
    }

    public Optional<User> findCustomer(final String searchTerm){
        return users.stream().filter(c -> c.getId().equals(searchTerm)).findAny();
    }
    public void removeCustomer(final String searchTerm){
        users.removeIf(p -> (findCustomer(searchTerm).isPresent()));
    }
    public void addToken(String a){
        tokens.add(a);
    }

    public Optional<String> findToken(final String searchTerm){
        return tokens.stream().filter(t -> t.equals(searchTerm)).findAny();
    }
    public void removeToken(final String searchTerm){
        tokens.removeIf(p -> (findToken(searchTerm).isPresent()));
    }

}
