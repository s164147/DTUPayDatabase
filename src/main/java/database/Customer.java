package database;

import database.Account;
import database.Merchant;
import database.User;
import database.invalidAmount;

import java.security.SecureRandom;
import java.util.Collections;
import java.util.*;

public class Customer extends User {

    public Account account = new Account(100);
    private LinkedList<String> tokens = new LinkedList<String>();

    public Customer(String name, String id) {
        super(name, id);
    }

    public String getToken() throws NoSuchElementException {
        return tokens.removeFirst();
    }

    public void setTokens(LinkedList<String> tokens) {
        this.tokens = tokens;
    }


    public LinkedList<String> getTokens() {
        return tokens;
    }


    public void addToken(String[] tokensToAdd) {
        Collections.addAll(tokens,tokensToAdd);
    }

    public void transferMoney(float amount, Merchant recipient) throws invalidAmount {
        account.removeMoney(amount);
        recipient.account.addMoney(amount);
    }
}
